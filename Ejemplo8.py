#Cree un diagrama de flujo que permita leer tres valores y almacenarlos en las
#variables A, B y C respectivamente. El algoritmo debe imprimir cual es el mayor y cual es
#el menor. Recuerde constatar que los tres valores introducidos por el teclado sean
#valores distintos. Presente un mensaje de error en caso de que se detecte la
#introducción de valores iguales, y proceda a terminar la ejecución del mismo

ValorA=int(input("Ingrese el primer valor:"))
ValorB=int(input("Ingrese el segundo valor:"))
ValorC=int(input("Ingrese el tercer valor:"))

if ValorA==ValorB and ValorC:
    print("Los tres son iguales")

elif ValorA>ValorB and ValorC:
    if ValorB>ValorC:
        print("ValorA es el mayor y ValorC es el menor")
    else:
        print("ValorA es el mayor y ValorB es el menor")

elif ValorB>ValorA and ValorC:
    if ValorA>ValorC:
        print("ValorB es el mayor y ValorC es el menor")
    else:
        print("ValorB es el mayor y ValorA es el menor")

elif ValorC>ValorA and ValorB:
    if ValorA>ValorB:
        print("ValorC es el mayor y ValorB es el menor")
    else:
        print("ValorC es el mayor y ValorA es el menor")
